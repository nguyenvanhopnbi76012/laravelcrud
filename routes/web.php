<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TenantController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// Route::resource('/tenants', [TenantController::class, 'index'])->name('tenants.index');

// Route::resource('/tenants', [App\Http\Controllers\TenantController::class])->name('tenants.index');

// Middleware user login

Route::group(['middleware' => 'auth'], function(){
    Route::resource('tenants', TenantController::class);
});

